FROM elixir:1.15.6-alpine

WORKDIR /code

RUN mix local.rebar --force
RUN mix local.hex --force

CMD mix deps.get && \
  mix ecto.create && \
  mix ecto.migrate && \
  mix run --no-halt
