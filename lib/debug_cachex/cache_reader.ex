defmodule DebugCachex.CacheReader do
  use GenServer

  require Logger

  import Ecto.Query

  alias DebugCachex.Repo
  alias DebugCachex.Stuff

  @impl GenServer
  def init(opts = %{name: name}) do
    Logger.metadata(name: name)
    Logger.debug("CacheReader.init(#{inspect(opts)})")

    Process.send_after(self(), :read, 1)
    {:ok, opts}
  end

  @impl GenServer
  def handle_info(:read, state = %{cache: cache, keys: keys}) do
    Logger.debug("CacheReader.handle_info(:read, #{inspect(state)})")

    key = Enum.random(keys)
    Logger.debug("reading #{key}")

    {status, %Stuff{id: id}} =
      Cachex.fetch(cache, key, fn key ->
        stuff =
          from(p in Stuff, where: p.name == ^key)
          |> Repo.one!()

        {:commit, stuff}
      end)

    Logger.info("#{status}: #{key} = #{id}")

    Process.send_after(self(), :read, Enum.random(1..100))
    {:noreply, state}
  end

  # client

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end
end
