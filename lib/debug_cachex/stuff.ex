defmodule DebugCachex.Stuff do
  use Ecto.Schema

  schema "stuff" do
    field(:name, :string)
    timestamps()
  end
end
