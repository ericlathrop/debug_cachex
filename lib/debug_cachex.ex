defmodule DebugCachex do
  @moduledoc """
  Documentation for `DebugCachex`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> DebugCachex.hello()
      :world

  """
  def hello do
    :world
  end
end
