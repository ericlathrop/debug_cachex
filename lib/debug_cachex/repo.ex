defmodule DebugCachex.Repo do
  use Ecto.Repo,
    otp_app: :debug_cachex,
    adapter: Ecto.Adapters.Postgres
end
