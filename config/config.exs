# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
import Config

config :logger, :console,
  level: :info,
  format: "$date $time [$level] $metadata$message\n",
  metadata: [:name]

config :debug_cachex, ecto_repos: [DebugCachex.Repo]
config :debug_cachex, env: config_env()

config :debug_cachex, DebugCachex.Repo, url: "ecto://postgres:postgres@postgres/postgres"
