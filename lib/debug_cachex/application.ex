defmodule DebugCachex.Application do
  @moduledoc false

  use Application

  import Cachex.Spec

  alias DebugCachex.Repo
  alias DebugCachex.Stuff

  @cache_size 10
  @cache_ttl_ms 10
  @number_of_keys 100
  @number_of_readers 100

  @impl true
  def start(_type, _args) do
    children = [
      {Cachex,
       expiration: expiration(default: @cache_ttl_ms),
       limit: limit(size: @cache_size, policy: Cachex.Policy.LRW, reclaim: 0.25, options: []),
       name: :stuff},
      DebugCachex.Repo
    ]

    opts = [strategy: :one_for_one, name: DebugCachex.Supervisor]
    {:ok, supervisor} = Supervisor.start_link(children, opts)

    keys = setup_database(@number_of_keys)

    Enum.each(1..@number_of_readers, fn id ->
      {:ok, _child} =
        Supervisor.start_child(supervisor, %{
          id: id,
          start: {DebugCachex.CacheReader, :start_link, [%{cache: :stuff, name: id, keys: keys}]}
        })
    end)

    {:ok, supervisor}
  end

  defp setup_database(n) do
    Repo.delete_all(Stuff)
    keys = Enum.map(1..n, fn _ -> string_of_length(20) end)
    Enum.map(keys, fn key -> Repo.insert(%Stuff{name: key}) end)
    keys
  end

  @chars "ABCDEFGHIJKLMNOPQRSTUVWXYZ" |> String.split("")

  defp string_of_length(length) do
    Enum.reduce(1..length, [], fn _i, acc ->
      [Enum.random(@chars) | acc]
    end)
    |> Enum.join("")
  end
end
