defmodule DebugCachex.Repo.Migrations.CreateStuff do
  use Ecto.Migration

  def change do
    create table(:stuff) do
      add(:name, :string, null: false)
      timestamps()
    end
  end
end
